﻿using System.Collections.Generic;
using ExpertSystemsProject.Models;

namespace ExpertSystemsProject.Comparers
{
    public class ParametersComparer:IEqualityComparer<Parameter>
    {
        public bool Equals(Parameter x, Parameter y)
        {
            if (x == null || y == null)
                return false;

            return x.idParameter == y.idParameter && x.Name == y.Name;
        }

        public int GetHashCode(Parameter obj)
        {
            if (obj == null)
                return 0;

            return (obj.idParameter.GetHashCode()) ^
                (obj.Name == null ? 0 : obj.Name.GetHashCode());
        }
    }
}