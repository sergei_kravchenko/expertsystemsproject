﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using ExpertSystemsProject.Comparers;
using ExpertSystemsProject.Models;
using ExpertSystemsProject.Models.Enums;
using NCalc;

namespace ExpertSystemsProject.Controllers
{
    public class Expertise
    {
        private readonly ExpertiseModel _expertiseModel;
        public Expertise(ExpertiseModel expertiseModel)
        {
            _expertiseModel =  expertiseModel;
        }

        /// <summary>
        /// Возвращает влияющие параметры.
        /// </summary>
        /// <param name="parameter">Параметр</param>
        /// <returns>Массив id влияющих параметров</returns>
        public static int[] GetInfluencingParametersIDs(Parameter parameter)
        {
            // Определяем регулярное выражение.
            var rx = new Regex(@"\d+");
            // Ищем совпадения.
            MatchCollection matches = rx.Matches(parameter.InfluencingParameters);
            var influencingParameters = new int[matches.Count];

            if (matches.Count > 0)
            {

                for (int i = 0; i < matches.Count; i++)
                {
                    influencingParameters[i] = Convert.ToInt32(matches[i].Value);
                }
            }

            return influencingParameters;
        }

        /// <summary>
        /// Возвращает влияющие параметры.
        /// </summary>
        /// <param name="parameter">Параметр</param>
        /// <returns>Массив id влияющих параметров</returns>
        public List<Parameter> GetInfluencingParameters(Parameter parameter)
        {
            var influencingParameters = new List<Parameter>();
            var influencingParametersIds = ParseArrayOfIntFromString(parameter.InfluencingParameters);

            using (var db = new ExpertSystemDBEntities())
            {
                foreach (var influencingParametersId in influencingParametersIds)
                {
                    influencingParameters.Add(db.Parameter.Find(influencingParametersId));
                }
            }

            return influencingParameters;
        }

        /// <summary>
        /// Парсит строку с числами.
        /// </summary>
        /// <param name="stringWithInt">Строка с числами.</param>
        /// <returns>Массив чисел.</returns>
        public int[] ParseArrayOfIntFromString(string stringWithInt)
        {
            int[] resultArray = null;

            // Определяем регулярное выражение.
            var rx = new Regex(@"\d+");
            // Ищем совпадения.
            var matches = rx.Matches(stringWithInt);

            if (matches.Count > 0)
            {
                resultArray = new int[matches.Count];

                for (int i = 0; i < matches.Count; i++)
                {
                    resultArray[i] = Convert.ToInt32(matches[i].Value);
                }
            }
            return resultArray;
        }

        /// <summary>
        /// Возвращает путь вывода.
        /// </summary>
        /// <param name="targetParameter">Целевой параметр.</param>
        /// <returns>Путь вывода(список параметров).</returns>
        private static IEnumerable<Parameter> GetOutputPathFromTargetParameter(Parameter targetParameter)
        {
            var outputPath = new List<Parameter>();
            //Инициализируем БД.
            var db = new ExpertSystemDBEntities();
            //Добавляем в путь целевой параметр.
            outputPath.Add(targetParameter);
            //Получаем id влиящих параметров.
            var influencingParametersIDs = GetInfluencingParametersIDs(targetParameter);
            //Для каждого id влияющего параметра
            foreach (var influencingParameterId in influencingParametersIDs)
            {
                //Получаем сам объект влияющего параметра.
                var influencingParameter = db.Parameter.Find(influencingParameterId);

                var sourceRelationType = Enum.GetName(typeof(ParameterRelationTypeEnum), ParameterRelationTypeEnum.Source);

                //Если получили
                if (influencingParameter != null)
                {
                    //Если тип связи исток
                    if (influencingParameter.ParameterRelationType.RelationType == sourceRelationType)
                    {
                        //Если влияющий параметр не включен в путь вывода
                        if (!outputPath.Contains(influencingParameter))
                        {
                            //добавляем в путь вывода
                            outputPath.Add(influencingParameter);
                        }
                    }
                    else
                    {
                        //Рекурсивно ищем влияющие параметры этого параметра и добавляем результат в путь вывода.
                        outputPath.AddRange(GetOutputPathFromTargetParameter(influencingParameter));
                    }
                }
            }
            return outputPath;
        }

        public IEnumerable<List<Parameter>> GetOutputPathHead(Parameter parameter)
        {
            var outputPaths = new List<List<Parameter>>();

            using (var dataBase = new ExpertSystemDBEntities())
            {
                var influencingParametersIDs = GetInfluencingParametersIDs(parameter);
                outputPaths.Add(new List<Parameter>() { parameter });
                var sourceRelationType = Enum.GetName(typeof(ParameterRelationTypeEnum), ParameterRelationTypeEnum.Source);

                foreach (var influencingParameterID in influencingParametersIDs)
                {
                    var influencingParameter = dataBase.Parameter.Find(influencingParameterID);
                    if (influencingParameter.ParameterRelationType.RelationType == sourceRelationType)
                    {
                        outputPaths.ForEach(path => path.Add(influencingParameter));
                        continue;
                    }

                    // Получаем параметры с таким же именем.
                    var parametersWithTheSameName = dataBase.Parameter
                        .Where(p => (p.Name == influencingParameter.Name)
                            && (p.idParameter != influencingParameter.idParameter))
                            .ToList();

                    if ((parametersWithTheSameName.Count > 0))
                    {
                        foreach (var parameterWithTheSameName in parametersWithTheSameName)
                        {
                            var innerOutputPaths = GetOutputPathHead(parameterWithTheSameName);
                            foreach (var innerOutputPath in innerOutputPaths)
                            {
                                outputPaths.Add(new List<Parameter>(outputPaths[0]));
                                outputPaths.Last().AddRange(innerOutputPath);
                            }
                        }
                    }

                    outputPaths[0].AddRange(GetOutputPathFromTargetParameter(influencingParameter));
                }
            }
            return outputPaths;
        }

        public List<List<Parameter>> RemoveParametersDublicates(List<List<Parameter>> outputPaths)
        {
            var distinctedOutputPaths = new List<List<Parameter>>();
            for (int i = 0; i < outputPaths.Count; i++)
            {
                distinctedOutputPaths.Add(outputPaths[i].Distinct(new ParametersComparer()).ToList());
                //distinctedOutputPaths.Add(new List<Parameter>(outputPaths[i].Remove(query ).Distinct()));
            }
            return distinctedOutputPaths;
        }

        /// <summary>
        /// Возвращает все пути вывода.
        /// </summary>
        /// <param name="targetParameter">Целевой параметр.</param>
        /// <returns>Пути вывода.</returns>
        public List<List<Parameter>> GetAllOutputPaths(List<Parameter> targetParameter)
        {
            var outputPaths = new List<List<Parameter>>();
            for (int i = 0; i < targetParameter.Count(); i++)
            {
                outputPaths.AddRange(GetOutputPathHead(targetParameter[i]));
            }
            return outputPaths;
        }

        /// <summary>
        /// Получает все возможные значения для параметра. 
        /// </summary>
        /// <returns>Словарь со всеми возможными значениями для параметра</returns>
        public Dictionary<Parameter, List<ParameterValue>>[] GetParametersValuesDictionaries()
        {
            var parametersValuesDictionary = new Dictionary<Parameter, List<ParameterValue>>[_expertiseModel.OutputPaths.Count];
            using (var db = new ExpertSystemDBEntities())
            {
                for (int i = 0; i < _expertiseModel.OutputPaths.Count; i++)
                {
                    parametersValuesDictionary[i] = new Dictionary<Parameter, List<ParameterValue>>();
                    //Для каждого параметра в пути вывода
                    foreach (var parameter in _expertiseModel.OutputPaths[i])
                    {
                        //Получаем список значений параметра.
                        var parameterValue = db.ParameterValue
                            .Where(parameter1 => parameter1.Parameter_idParameter == parameter.idParameter)
                            .ToList();
                        parametersValuesDictionary[i].Add(parameter, parameterValue);
                    }
                }
            }
            return parametersValuesDictionary;
        }

        /// <summary>
        /// Выдает значения параметра по значениям параметров от которого он зависит.
        /// </summary>
        /// <param name="parameter">Параметр.</param>
        /// <param name="outputPathIndex">Индекс пути вывода.</param>
        /// <returns>Значения параметра.</returns>
        public List<ParameterValue> GetParameterResults(Parameter parameter, int outputPathIndex)
        {
            // "Source"
            var sourceRelationType = Enum.GetName(typeof(ParameterRelationTypeEnum), ParameterRelationTypeEnum.Source);
            var formulaRelationType = Enum.GetName(typeof(ParameterRelationTypeEnum), ParameterRelationTypeEnum.Formula);

            var parameterValues = new List<ParameterValue>();

            // Если тип параметра источник
            if (parameter.ParameterRelationType.RelationType == sourceRelationType)
            {
                    // Если словарь содержит параметер, возвращаем его значение.
                    if (_expertiseModel.SelectedParametersValues[outputPathIndex].ContainsKey(parameter))
                        parameterValues.Add(_expertiseModel.SelectedParametersValues[outputPathIndex][parameter]);
            }
            else
            {
                var influencingParametersValues = new List<ParameterValue>();

                // Для каждого влияющего параметра получаем его выбранное значение.
                foreach (var influencingParameter in GetInfluencingParameters(parameter))
                {
                        foreach (var parameterValue in _expertiseModel.SelectedParametersValues[outputPathIndex])
                        {
                            if (parameterValue.Key.Name == influencingParameter.Name)
                            {
                                influencingParametersValues.Add(parameterValue.Value);
                                break;
                            }
                        }
                }

                using (var db = new ExpertSystemDBEntities())
                {
                    // Получаем условия которые относятся к параметру.
                    var clauses = db.Clause.Where(clause => clause.Parameter_idParameter == parameter.idParameter);

                    if (parameter.ParameterRelationType.RelationType == formulaRelationType)
                    {
                        var formula = clauses.FirstOrDefault();
                        if (formula != null)
                        {
                            var e = new Expression(formula.Clause1);

                            for (int i = 0; i < influencingParametersValues.Count; i++)
                            {
                                e.Parameters["p" + (i+1)] = Convert.ToDouble(influencingParametersValues[i].Value);
                            }

                            if (outputPathIndex == 0)
                                parameterValues
                                    .Add( new ParameterValue {Parameter = parameter, 
                                            Value = e.Evaluate().ToString()} );
                            else
                            {
                                var rootParameter = db.Parameter.First(p => p.Name == parameter.Name);
                                parameterValues
                                    .Add(new ParameterValue { Parameter_idParameter = rootParameter.idParameter,
                                            Value = e.Evaluate().ToString() });
                            }
                        }
                    }
                    else
                    {
                        // Для каждого условия
                        foreach (var clause in clauses)
                        {
                            // Получаем массив порядковых номеров значений необходимых для выполнения условия условия.
                            var valuesSequenceNumbers = ParseArrayOfIntFromString(clause.Clause1);
                            var isClauseWeNeeded = true;

                            // Если число значений параметров в условии не равно числу выбранных значений, то пропускаем условие.
                            if (valuesSequenceNumbers.Length != influencingParametersValues.Count)
                                continue;

                            // Для каждого значения влияющего параметра 
                            for (int i = 0; i < influencingParametersValues.Count; i++)
                            {
                                // Если значение параметра не равно выбранному значению, то пропускаем это условие.
                                if (influencingParametersValues[i].SequenceNumber != valuesSequenceNumbers[i])
                                {
                                    // Если порядковый номер - любое число
                                    if (valuesSequenceNumbers[i] == 9999)
                                    {
                                        continue;
                                    }
                                    isClauseWeNeeded = false;
                                    break;
                                }
                            }

                            if (isClauseWeNeeded)
                            {
                                if (outputPathIndex == 0)
                                    parameterValues
                                        .Add(db.ParameterValue
                                        .Include(p => p.Parameter)
                                        .First(parameterValue =>
                                            (parameterValue.Parameter_idParameter == parameter.idParameter) &&
                                            (parameterValue.SequenceNumber == clause.Result_SequenceNumber)));
                                else
                                {
                                    var rootParameter = db.Parameter.First(p => p.Name == parameter.Name);
                                    parameterValues
                                        .Add(db.ParameterValue
                                        .Include(p => p.Parameter)
                                        .First(parameterValue =>
                                            (parameterValue.Parameter_idParameter == rootParameter.idParameter) &&
                                            (parameterValue.SequenceNumber == clause.Result_SequenceNumber)));
                                }
                            }
                        }
                    }
                }
            }
            return parameterValues;
        }

        /// <summary>
        /// Получает результат экспертизы для пути вывода.
        /// </summary>
        /// <param name="outputPathIndex">Индекс пути вывода.</param>
        /// <returns>Значение резултирующего параметра.</returns>
        public ParameterValue GetResult(int outputPathIndex)
        {
            // "Source" 
            var sourceRelationType = Enum.GetName(typeof(ParameterRelationTypeEnum), ParameterRelationTypeEnum.Source);
            
            // Для каждого параметра в пути вывода
            foreach (Parameter parameter in new List<Parameter>(_expertiseModel.OutputPaths[outputPathIndex]))
            {
                // Если для параметра уже выбрано значение, то пропускаем его. 
                if (_expertiseModel.SelectedParametersValues[outputPathIndex].ContainsKey(parameter))
                    continue;

                // Если тип параметра не исток
                if (parameter.ParameterRelationType.RelationType != sourceRelationType)
                {
                    var parameterResults = GetParameterResults(parameter, outputPathIndex);
                    if (parameterResults.Count == 1)
                    {
                        //Добавляем значение параметра в словарь значений.
                        _expertiseModel.SelectedParametersValues[outputPathIndex]
                            .Add(parameter, parameterResults[0]);
                    }
                    else
                    {
                        for (int i = 1; i < parameterResults.Count; i++)
                        {
                            var createdOutputPathIndex = AddOutputPathCopy(outputPathIndex);
                            _expertiseModel
                                .SelectedParametersValues[createdOutputPathIndex].Add(parameter, parameterResults[i]);
                            GetResult(createdOutputPathIndex);
                        }

                        //Добавляем значение параметра в словарь значений.
                        _expertiseModel.SelectedParametersValues[outputPathIndex]
                            .Add(parameter, parameterResults[0]);
                    }
                }
            }
            return _expertiseModel.SelectedParametersValues[outputPathIndex].Last().Value;
        }

        /// <summary>
        /// Добавляет копию пути вывода и выбранные значения.
        /// </summary>
        /// <param name="outputPathIndex">Индекс пути вывода.</param>
        /// <returns>Индекс созданного пути.</returns>
        private int AddOutputPathCopy(int outputPathIndex)
        {
            _expertiseModel.OutputPaths.Add(new List<Parameter>(_expertiseModel.OutputPaths[outputPathIndex]));
            _expertiseModel.SelectedParametersValues
                .Add(new Dictionary<Parameter, ParameterValue>(_expertiseModel.SelectedParametersValues[outputPathIndex]));
            return _expertiseModel.OutputPaths.Count - 1;
        }

        public List<string> GetExplanation(int outputPathIndex)
        {
            var explanation = new List<string>();

            for (int i = 0; i < _expertiseModel.OutputPaths[outputPathIndex].Count; i++)
            {
                explanation.Add(_expertiseModel.OutputPaths[outputPathIndex][i].Name + " = " +
                                 _expertiseModel.SelectedParametersValues[outputPathIndex][_expertiseModel
                                 .OutputPaths[outputPathIndex][i]].Value);
            }

            return explanation;
        }

    }
}