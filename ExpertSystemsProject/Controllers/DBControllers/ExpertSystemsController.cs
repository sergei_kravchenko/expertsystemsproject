﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ExpertSystemsProject.Models;

namespace ExpertSystemsProject.Controllers.DBControllers
{
    public class ExpertSystemsController : BaseController, IDbController
    {
        private readonly ExpertSystemDBEntities _db = new ExpertSystemDBEntities();

        // GET: /ExpertSystems/
        public ActionResult Index()
        {
            List<ExpertSystem> expertSystemsList;
            try
            {
                expertSystemsList = _db.ExpertSystem.ToList();
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }

            return View(expertSystemsList);
        }

        // GET: /ExpertSystems/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ExpertSystem expertsystem;
            try
            {
                expertsystem = _db.ExpertSystem.Find(id);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }

            if (expertsystem == null)
            {
                return HttpNotFound();
            }
            return View(expertsystem);
        }

        // GET: /ExpertSystems/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /ExpertSystems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="idExpertSystem,Name,Description")] ExpertSystem expertsystem)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _db.ExpertSystem.Add(expertsystem);
                    _db.SaveChanges();
                }
                catch (Exception)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
                }
                return RedirectToAction("Index");
            }

            return View(expertsystem);
        }

        // GET: /ExpertSystems/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ExpertSystem expertsystem;
            try
            {
                expertsystem = _db.ExpertSystem.Find(id);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }

            if (expertsystem == null)
            {
                return HttpNotFound();
            }
            return View(expertsystem);
        }

        // POST: /ExpertSystems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="idExpertSystem,Name,Description")] ExpertSystem expertsystem)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _db.Entry(expertsystem).State = EntityState.Modified;
                    _db.SaveChanges();
                }
                catch (Exception)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
                }
                return RedirectToAction("Index");
            }
            return View(expertsystem);
        }

        // GET: /ExpertSystems/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ExpertSystem expertsystem;
            try
            {
                expertsystem = _db.ExpertSystem.Find(id);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }

            if (expertsystem == null)
            {
                return HttpNotFound();
            }
            return View(expertsystem);
        }

        // POST: /ExpertSystems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                ExpertSystem expertsystem = _db.ExpertSystem.Find(id);
                _db.ExpertSystem.Remove(expertsystem);
                _db.SaveChanges();

            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
