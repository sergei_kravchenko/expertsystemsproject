﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ExpertSystemsProject.Models;
using Parameter = ExpertSystemsProject.Models.Parameter;

namespace ExpertSystemsProject.Controllers.DBControllers
{
    public class ParametersController : BaseController, IDbController
    {
        private readonly ExpertSystemDBEntities _db = new ExpertSystemDBEntities();

        // GET: /Parameters/
        public ActionResult Index()
        {
            List<Parameter> parameter;
            try
            {
                parameter = _db.Parameter.Include(p => p.ExpertSystem)
                    .Include(p => p.ParameterRelationType).Include(p => p.ParameterType).ToList();
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }

            return View(parameter);
        }

        // GET: /Parameters/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Parameter parameter;
            try
            {
                parameter = _db.Parameter.Find(id);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }

            if (parameter == null)
            {
                return HttpNotFound();
            }
            return View(parameter);
        }

        // GET: /Parameters/Create
        public ActionResult Create()
        {
            ViewBag.ExpertSystem_idExpertSystem = new SelectList(_db.ExpertSystem, "idExpertSystem", "Name");
            ViewBag.ParameterRelationType_idRelationType = new SelectList(_db.ParameterRelationType, "idRelationType", "RelationType");
            ViewBag.ParameterType_idParameterType = new SelectList(_db.ParameterType, "idParameterType", "Type");
            return View();
        }

        // POST: /Parameters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="idParameter,Name,ParameterType_idParameterType,ParameterRelationType_idRelationType,InfluencingParameters,ExpertSystem_idExpertSystem")] Parameter parameter)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _db.Parameter.Add(parameter);
                    _db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.ExpertSystem_idExpertSystem = new SelectList(_db.ExpertSystem, "idExpertSystem", "Name", parameter.ExpertSystem_idExpertSystem);
                ViewBag.ParameterRelationType_idRelationType = new SelectList(_db.ParameterRelationType, "idRelationType", "RelationType", parameter.ParameterRelationType_idRelationType);
                ViewBag.ParameterType_idParameterType = new SelectList(_db.ParameterType, "idParameterType", "Type", parameter.ParameterType_idParameterType);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }

            return View(parameter);
        }

        // GET: /Parameters/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Parameter parameter;
            try
            {
                parameter = _db.Parameter.Find(id);
                if (parameter == null)
                {
                    return HttpNotFound();
                }
                ViewBag.ExpertSystem_idExpertSystem = new SelectList(_db.ExpertSystem, "idExpertSystem", "Name", parameter.ExpertSystem_idExpertSystem);
                ViewBag.ParameterRelationType_idRelationType = new SelectList(_db.ParameterRelationType, "idRelationType", "RelationType", parameter.ParameterRelationType_idRelationType);
                ViewBag.ParameterType_idParameterType = new SelectList(_db.ParameterType, "idParameterType", "Type", parameter.ParameterType_idParameterType);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }

            return View(parameter);
        }

        // POST: /Parameters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="idParameter,Name,ParameterType_idParameterType,ParameterRelationType_idRelationType,InfluencingParameters,ExpertSystem_idExpertSystem")] Parameter parameter)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _db.Entry(parameter).State = EntityState.Modified;
                    _db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.ExpertSystem_idExpertSystem = new SelectList(_db.ExpertSystem, "idExpertSystem", "Name", parameter.ExpertSystem_idExpertSystem);
                ViewBag.ParameterRelationType_idRelationType = new SelectList(_db.ParameterRelationType, "idRelationType", "RelationType", parameter.ParameterRelationType_idRelationType);
                ViewBag.ParameterType_idParameterType = new SelectList(_db.ParameterType, "idParameterType", "Type", parameter.ParameterType_idParameterType);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }
            return View(parameter);
        }

        // GET: /Parameters/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Parameter parameter;
            try
            {
                parameter = _db.Parameter.Find(id);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }

            if (parameter == null)
            {
                return HttpNotFound();
            }
            return View(parameter);
        }

        // POST: /Parameters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Parameter parameter = _db.Parameter.Find(id);
                _db.Parameter.Remove(parameter);
                _db.SaveChanges();
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
