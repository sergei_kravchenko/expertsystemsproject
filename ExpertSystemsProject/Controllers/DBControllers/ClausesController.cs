﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ExpertSystemsProject.Models;

namespace ExpertSystemsProject.Controllers.DBControllers
{
    public class ClausesController : BaseController, IDbController
    {
        private readonly ExpertSystemDBEntities _db = new ExpertSystemDBEntities();

        // GET: /Clauses/
        public ActionResult Index()
        {
            var clause = _db.Clause.Include(c => c.Parameter);
            return View(clause.ToList());
        }

        // GET: /Clauses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Clause clause = _db.Clause.Find(id);
            if (clause == null)
            {
                return HttpNotFound();
            }
            return View(clause);
        }

        // GET: /Clauses/Create
        public ActionResult Create()
        {
            ViewBag.Parameter_idParameter = new SelectList(_db.Parameter, "idParameter", "Name");
            return View();
        }

        // POST: /Clauses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="idClause,Clause1,Parameter_idParameter,Result_SequenceNumber,CertaintyFactor")] Clause clause)
        {
            if (ModelState.IsValid)
            {
                _db.Clause.Add(clause);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Parameter_idParameter = new SelectList(_db.Parameter, "idParameter", "Name", clause.Parameter_idParameter);
            return View(clause);
        }

        // GET: /Clauses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Clause clause = _db.Clause.Find(id);
            if (clause == null)
            {
                return HttpNotFound();
            }
            ViewBag.Parameter_idParameter = new SelectList(_db.Parameter, "idParameter", "Name", clause.Parameter_idParameter);
            return View(clause);
        }

        // POST: /Clauses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="idClause,Clause1,Parameter_idParameter,Result_SequenceNumber,CertaintyFactor")] Clause clause)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(clause).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Parameter_idParameter = new SelectList(_db.Parameter, "idParameter", "Name", clause.Parameter_idParameter);
            return View(clause);
        }

        // GET: /Clauses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Clause clause = _db.Clause.Find(id);
            if (clause == null)
            {
                return HttpNotFound();
            }
            return View(clause);
        }

        // POST: /Clauses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Clause clause = _db.Clause.Find(id);
            _db.Clause.Remove(clause);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
