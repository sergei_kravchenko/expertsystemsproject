﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ExpertSystemsProject.Models;

namespace ExpertSystemsProject.Controllers.DBControllers
{
    public class ParametersValuesController : BaseController, IDbController
    {
        private readonly ExpertSystemDBEntities _db = new ExpertSystemDBEntities();

        // GET: /ParametersValues/
        public ActionResult Index()
        {
            IQueryable<ParameterValue> parametervalue;
            try
            {
                parametervalue = _db.ParameterValue.Include(p => p.Parameter);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }

            return View(parametervalue);
        }

        // GET: /ParametersValues/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ParameterValue parametervalue;
            try
            {
                parametervalue = _db.ParameterValue.Find(id);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }

            if (parametervalue == null)
            {
                return HttpNotFound();
            }
            return View(parametervalue);
        }

        // GET: /ParametersValues/Create
        public ActionResult Create()
        {
            try
            {
                ViewBag.Parameter_idParameter = new SelectList(_db.Parameter, "idParameter", "Name");
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }

            return View();
        }

        // POST: /ParametersValues/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="idParameterValue,Parameter_idParameter,SequenceNumber,Value")] ParameterValue parametervalue)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _db.ParameterValue.Add(parametervalue);
                    _db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.Parameter_idParameter = new SelectList(_db.Parameter, "idParameter", "Name", parametervalue.Parameter_idParameter);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }

            return View(parametervalue);
        }

        // GET: /ParametersValues/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ParameterValue parametervalue;
            try
            {
                parametervalue = _db.ParameterValue.Find(id);
                if (parametervalue == null)
                {
                    return HttpNotFound();
                }
                ViewBag.Parameter_idParameter = new SelectList(_db.Parameter, "idParameter", "Name", parametervalue.Parameter_idParameter);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }

            return View(parametervalue);
        }

        // POST: /ParametersValues/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="idParameterValue,Parameter_idParameter,SequenceNumber,Value")] ParameterValue parametervalue)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _db.Entry(parametervalue).State = EntityState.Modified;
                    _db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.Parameter_idParameter = new SelectList(_db.Parameter, "idParameter", "Name", parametervalue.Parameter_idParameter);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }
            return View(parametervalue);
        }

        // GET: /ParametersValues/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ParameterValue parametervalue;
            try
            {
                parametervalue = _db.ParameterValue.Find(id);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }

            if (parametervalue == null)
            {
                return HttpNotFound();
            }
            return View(parametervalue);
        }

        // POST: /ParametersValues/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                ParameterValue parametervalue = _db.ParameterValue.Find(id);
                _db.ParameterValue.Remove(parametervalue);
                _db.SaveChanges();
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
