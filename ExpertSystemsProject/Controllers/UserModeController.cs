﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ExpertSystemsProject.Models;
using ExpertSystemsProject.Models.Enums;


namespace ExpertSystemsProject.Controllers
{
    public class UserModeController : BaseController
    {
        private readonly ExpertSystemDBEntities _db = new ExpertSystemDBEntities();
        private const string SessionExpertiseModelName = "ExpertiseModel";

        private ExpertiseModel ExpertiseModel
        {
            get
            {
                if (System.Web.HttpContext.Current.Session[SessionExpertiseModelName] == null)
                {
                    System.Web.HttpContext.Current.Session.Add(SessionExpertiseModelName, new ExpertiseModel());
                }
                return System.Web.HttpContext.Current.Session[SessionExpertiseModelName] as ExpertiseModel;
            }
            set
            {
                if (System.Web.HttpContext.Current.Session[SessionExpertiseModelName] == null)
                {
                    System.Web.HttpContext.Current.Session.Add(SessionExpertiseModelName, value);
                }
                else
                {
                    System.Web.HttpContext.Current.Session[SessionExpertiseModelName] = value;
                }
            }
        }

        private readonly Expertise _expertise;

        public UserModeController()
        {
            _expertise = new Expertise(ExpertiseModel);
        }

        //
        // GET: /UserMode/
        public ActionResult SelectExpertSystem(int? id)
        {
            if (id != null)
            {
                ViewBag.SelectedExpertSystemID = id.Value;
            }

            List<ExpertSystem> expertSystemsList;
            try
            {
                expertSystemsList = _db.ExpertSystem.ToList();
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }

            return View(expertSystemsList);
        }

        //
        // GET: /UserMode/SelectTargetParameter
        public ActionResult SelectTargetParameter(int? expertSystemId, int? parameterId)
        {
            List<Parameter> parametersList;
            try
            {
                if (expertSystemId != null)
                {
                    //Заносим выбранную базу данных.
                    ExpertiseModel.ExpertSystem = _db.ExpertSystem.Find(expertSystemId);
                }

                if (parameterId != null)
                {
                    //Передаем виду Id выделенного элемента(для отрисовки).
                    ViewBag.SelectedParameterID = parameterId;
                }

                if ((expertSystemId == null) && (ExpertiseModel.ExpertSystem == null))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }


                //Загружаем данные из бд.
                var parameters = _db.Parameter.Include(p => p.ExpertSystem).Include(p => p.ParameterRelationType)
                    .Include(p => p.ParameterType);

                if (parameters == null)
                {
                    return HttpNotFound();
                }

                //Получаем не истоковые параметры заданной экспертной системы.
                parameters = parameters.Where(parameter1 => parameter1.ExpertSystem_idExpertSystem
                    == ExpertiseModel.ExpertSystem.idExpertSystem)
                    .Where(parameter1 => parameter1.ParameterRelationType_idRelationType != 1);

                try
                {
                    parametersList = parameters.ToList();
                }
                catch (Exception)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
                }
            }
            catch (EntityException)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }

            return View(parametersList);
        }

        //
        // GET: /UserMode/SelectTargetParameter
        public ActionResult FillSourceParameters(int? parameterId)
        {
            try
            {
                //Проверяем выбрана ли экспертная система(для запросов к БД)
                if (ExpertiseModel.ExpertSystem == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                //Проверяем выбран ли целевой параметр и Id параметра (для запросов к БД)
                if ((ExpertiseModel.TargetParameters == null) && (parameterId == null))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                try
                {
                    //Загружаем данные из бд.
                    var targetParameters = _db.Parameter.Include(p => p.ExpertSystem).Include(p => p.ParameterRelationType)
                        .Include(p => p.ParameterType);

                    if (targetParameters == null)
                    {
                        return HttpNotFound();
                    }
                    var targetParameter = _db.Parameter.Find(parameterId);
                    //Получаем не истоковые параметры заданной экспертной системы.
                    targetParameters = targetParameters.Where(parameter1 => parameter1.ExpertSystem_idExpertSystem
                        == ExpertiseModel.ExpertSystem.idExpertSystem)
                        .Where(parameter1 => parameter1.Name == targetParameter.Name);

                    ExpertiseModel.TargetParameters = targetParameters.ToList();
                    ExpertiseModel.OutputPaths = _expertise.GetAllOutputPaths(ExpertiseModel.TargetParameters);
                    ExpertiseModel.OutputPaths = _expertise.RemoveParametersDublicates(ExpertiseModel.OutputPaths);
                }
                catch (Exception)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
                }

                if (ExpertiseModel.OutputPaths == null)
                {
                    return HttpNotFound();
                }

                ViewBag.ParametersValuesDictionary = _expertise.GetParametersValuesDictionaries();
            }
            catch (EntityException)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }

            return View();
        }

        // POST: /Parameters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FillSourceParameters(List<List<int>> selectedParametersValuesIdList)
        {
            try
            {
                if (selectedParametersValuesIdList == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                }

                ExpertiseModel.SelectedParametersValues = new List<Dictionary<Parameter, ParameterValue>>();

                var parametersValuesDictionaryList = _expertise.GetParametersValuesDictionaries();

                for (int j = 0; j < parametersValuesDictionaryList.Length; j++)
                {
                    ExpertiseModel.SelectedParametersValues.Add(new Dictionary<Parameter, ParameterValue>());
                    int i = 0;
                    foreach (var dictionary in parametersValuesDictionaryList[j])
                    {
                        if (dictionary.Key.ParameterRelationType.RelationType ==
                            Enum.GetName(typeof(ParameterRelationTypeEnum), ParameterRelationTypeEnum.Source))
                        {
                            foreach (var parameterValue in dictionary.Value)
                            {
                                if (parameterValue.idParameterValue == selectedParametersValuesIdList[j][i])
                                {
                                    ExpertiseModel.SelectedParametersValues[j]
                                        .Add(dictionary.Key, parameterValue);
                                    break;
                                }
                            }
                            i++;
                        }
                    }
                }

                return RedirectToAction("Result");
                
            }
            catch (EntityException)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }
        }

        //
        // GET: /UserMode/SelectTargetParameter
        public ActionResult Result()
        {
            var resultList = new Dictionary<string,List<string>>();
            ExpertiseModel.Result = new List<ParameterValue>();
            var clauses = _db.Clause.ToList();

            try
            {
                if (ExpertiseModel.Result == null)
                    ExpertiseModel.Result = new List<ParameterValue>();


                for (int i = 0; i < ExpertiseModel.OutputPaths.Count; i++)
                {
                    //Упорядочиваем путь вывода
                    ExpertiseModel.OutputPaths[i].Reverse();

                    ExpertiseModel.Result.Add(_expertise.GetResult(i));
                    Clause resultClause;
                    if (ExpertiseModel.Result[i].Parameter.ParameterRelationType_idRelationType != 3)
                    {
                        resultClause =
                            clauses.FirstOrDefault(
                                clause =>
                                    (clause.Parameter_idParameter == ExpertiseModel.Result[i].Parameter.idParameter) &&
                                    (clause.Result_SequenceNumber == ExpertiseModel.Result[i].SequenceNumber));
                    }
                    else
                    {
                        resultClause =
                            clauses.FirstOrDefault(
                                clause =>
                                    clause.Parameter_idParameter == ExpertiseModel.Result[i].Parameter.idParameter);
                    }

                    resultList.Add(ExpertiseModel.Result[i].Parameter.Name + " = " + ExpertiseModel.Result[i].Value
                        + "  (CF=" + resultClause.CertaintyFactor + ")", _expertise.GetExplanation(i));
                }
            }
            catch (EntityException)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }

            //return View(resultList);
            return View(resultList);

        }

    }
}
