﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ExpertSystemsProject.Startup))]
namespace ExpertSystemsProject
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
