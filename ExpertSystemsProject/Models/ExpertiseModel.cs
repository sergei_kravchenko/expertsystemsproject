﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using System.Web;

namespace ExpertSystemsProject.Models
{
    public class ExpertiseModel
    {
        /// <summary>
        /// Экспертная система.
        /// </summary>
        public ExpertSystem ExpertSystem { get; set; }

        /// <summary>
        /// Список целевых параметров.
        /// </summary>
        public List<Parameter> TargetParameters { get; set; }

        /// <summary>
        /// Результат.
        /// </summary>
        public List<ParameterValue> Result { get; set; }

        /// <summary>
        /// Список путей вывода.
        /// </summary>
        public List<List<Parameter>> OutputPaths { get; set; }

        public List<Dictionary<Parameter, ParameterValue>> SelectedParametersValues { get; set;}

        public ExpertiseModel()
        {
            ExpertSystem = new ExpertSystem();
            TargetParameters = new List<Parameter>();
            OutputPaths = new List<List<Parameter>>();
            SelectedParametersValues = new List<Dictionary<Parameter, ParameterValue>>();
        }



        //public void  

    }
}