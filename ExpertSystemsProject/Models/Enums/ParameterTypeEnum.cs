﻿namespace ExpertSystemsProject.Models.Enums
{
    /// <summary>
    /// Перечисление типов параметров.
    /// </summary>
    public enum ParameterTypeEnum
    {
        Number,
        String
    }
}