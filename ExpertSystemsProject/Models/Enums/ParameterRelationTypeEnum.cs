﻿namespace ExpertSystemsProject.Models.Enums
{
    /// <summary>
    /// Перечисление типов отношений параметров.
    /// </summary>
    public enum ParameterRelationTypeEnum
    {
        Source, // Источник.
        Rule,   // Правила.
        Formula // Формула.
    }
}